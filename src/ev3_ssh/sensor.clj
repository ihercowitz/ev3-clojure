(ns ev3-ssh.sensor
  (:require [ev3-ssh.brick :refer [execute]]
            [clojure.core.async :refer [chan alts!! >!! thread]]))

(def p (agent 0))

(add-watch p :proximity (fn [k _ os ns] (print @p)))

(defn prx []
  (-> (execute "cat /sys/class/msensor/sensor4/value0")
        :out
        (clojure.string/replace #"\n" "")
        Integer/parseInt))

(defn prox-infinite []
  (.start (Thread.
           (do
             (loop [ov 0
                    nv 0]

               (when-not (= ov nv)
                 (println nv))
               (recur nv (prx))


               )))))

(defn proximity []
  (while true
    (-> (execute "cat /sys/class/msensor/sensor4/value0")
        :out
        (clojure.string/replace #"\n" "")
        prn)
    (Thread/sleep 1000)))
