(ns ev3-ssh.motor
  (:require [ev3-ssh.brick :refer [execute write]]))

(defonce ports {:a "outA"
                :b "outB"
                :c "outC"
                :d "outD"})

(def motor-location (atom {}))

(defn- find-motors []
  (-> (execute "ls /sys/class/tacho-motor/")
      :out
      (clojure.string/split #"\n")))

(defn- update-motors [motor]
  (let [motor-raw (:out (execute (str "cat /sys/class/tacho-motor/" motor "/port_name")))
        motor-out (clojure.string/replace motor-raw #"\n" "")]
    (swap! motor-location assoc motor-out motor)))

(defn motors []
  (doseq [m (find-motors)]
    (update-motors m)))

(defn motor [port]
  (motors)
  (->> (port ports)
       (get @motor-location)
       (str "/sys/class/tacho-motor/")))

(defn- motor-file [port file]
  (str (motor port) "/" file))

(defn velocity [port value]
  (execute (str "echo " value " > " (motor port)  "/duty_cycle")))

(defn run [port]
  (write (motor-file port "run") 1))




(defn stop [port]
  (write (motor-file port "run") 0))

(defn start [port]
  (doseq [cmd [(str "echo   off > "(motor port) "/regulation_mode")
               (str "echo   90 > " (motor port) "/duty_cycle_sp")]]
    (execute cmd)
    (run port)))

(defn- set-value [port file value]
  (str "echo " value " > " (motor port) "/" file))

(defn reset [port]
  (doseq [cmd [(set-value port "position" "0")
               (set-value port "run_mode" "position")
               (set-value port "stop_mode" "brake")
               (set-value port "regulation_mode" "on")
               (set-value port "ramp_up_sp" "300")
               (set-value port "ramp_down_sp" "300")
               (set-value port "pulses_per_second_sp" "800")
               (set-value port "position_sp" 0)]]
    (execute cmd)))

(defn stop-mode [port type]
  "The stop-mode determines how the motor will stop.
   There are 3 types: coast, brake and hold.

   Coast -
   Break -
   Hold  -"
  (-> (motor-file port "stop_mode")
      (write type)))
   
(defn run-mode [port type]
  "The run-mode determines how the motor is going to run.
   There are 2 types: forever or position

   Forever - As the name says, the motor runs forever
   Position - Runs the motor to a determined position"
  (-> (motor-file port "run_mode")
      (write type)))

(defn set-velocity [port velocity]
  (-> (motor-file port "pulses_per_second_sp")
      (write velocity)))

(defn rotate [port rotation]
  (write (motor-file port "position_sp") rotation)
  (run port))
