(ns ev3-ssh.led
  (:require [ev3-ssh.brick :refer [execute]]))

(defn leds [color side intensity]
  {:pre [(>= intensity 0) (<= intensity 100)]}
  (execute (str "echo " intensity " > /sys/class/leds/ev3\\" color "\\" side "/brightness")))


(def green (partial leds :green))
(def red   (partial leds :red))

(defn orange [side intensity]
  (green side intensity)
  (red side intensity))
