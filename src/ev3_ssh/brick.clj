(ns ev3-ssh.brick
  (:require [clj-ssh.cli :only [session default-session-options] :as cli]
            [clj-ssh.ssh :only [ssh with-connection connect connected?] :as ssh]))

(defn read-profile []
  (->> "project.clj"
        slurp
        read-string
        (drop 3)
        (partition 2)
        (map vec)
        (into {})
        :profiles))

(defn remote-info [prop]
  (get-in (read-profile) [:remote prop]))

(defn is-ssh? []
  (contains? (read-profile) :remote))

(def USERNAME (remote-info :username))
(def PASSWORD (remote-info :password))
(def BRICKIP  (remote-info :ip))
(def conn (cli/session BRICKIP :username USERNAME :password PASSWORD :strict-host-key-checking :no))


(defn connect [conn]
  (when-not (ssh/connected? conn)
    (ssh/connect conn)))

(defn execute [cmd]
  (connect conn)
  (ssh/ssh conn {:cmd cmd}))



(defn write [file value]
  (when (is-ssh?)
    (execute (str "echo " value " > " file))))
