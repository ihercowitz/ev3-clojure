(ns ev3-ssh.core
  (:require [ev3-ssh.motor :as motor]
            [ev3-ssh.sensor :as sensor]
            [ev3-ssh.led :as led]))


(defn open []
  (motor/rotate :b -360))


(defn close []
  (motor/rotate :b 30))
